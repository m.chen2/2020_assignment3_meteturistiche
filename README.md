# **Assignment 3 : MeteTuristiche**

*Chen Matthew - 816626*

# Breve descrizione dell'applicazione
L'applicazione permette di gestire un insieme di utenti e la loro interazione con delle mete turistiche e degli hotel.

Una volta clonato il repository, per eseguire l'applicazione è necessario eseguire la seguente riga di commando all'interno della cartella del progetto.
```
./mvnw spring-boot:run
```
Avviata l'applicazione vi sarà possibile accedervi all'indirizzo: *http://localhost:8181*.

Gli indirizzi principali saranno:

http://localhost:8181/mete 

http://localhost:8181/utenti 

    - http://localhost:8181/utenti/viaggiatori 

    - http://localhost:8181/utenti/guide 
    
http://localhost:8181/hotel 

