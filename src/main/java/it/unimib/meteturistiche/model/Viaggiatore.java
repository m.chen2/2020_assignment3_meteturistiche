package it.unimib.meteturistiche.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;


@Entity
@DiscriminatorValue("Viaggiatore")
public class Viaggiatore extends Utente {
	
	public Viaggiatore() { }
	
	@Column(name = "n_viaggi")
	private int n_viaggi;
	
	@ManyToMany
	@JoinTable(
			  name = "viaggiatore_meta", 
			  joinColumns = @JoinColumn(name = "viaggiatore_id"), 
			  inverseJoinColumns = @JoinColumn(name = "meta_id"))
	private List<MetaTuristica> mete_preferite;
	

	/**
	 * GETTERS AND SETTERS
	 */
	
	public int getN_viaggi() {
		return n_viaggi;
	}

	public void setN_viaggi(int n_viaggi) {
		this.n_viaggi = n_viaggi;
	}

	public List<MetaTuristica> getMete_preferite() {
		return mete_preferite;
	}

	public void setMete_preferite(List<MetaTuristica> mete_preferite) {
		this.mete_preferite = mete_preferite;
	}	

}
