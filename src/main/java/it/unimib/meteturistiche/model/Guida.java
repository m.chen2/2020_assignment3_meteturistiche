package it.unimib.meteturistiche.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Entity
@DiscriminatorValue("Guida")
public class Guida extends Utente {
	
	@Column
	private String agenzia;
	
	@Column
	private int anni_esperienza;
	
	@ManyToMany
	@JoinTable(
			  name = "guida_meta", 
			  joinColumns = @JoinColumn(name = "guida_id"), 
			  inverseJoinColumns = @JoinColumn(name = "meta_id"))
	private List <MetaTuristica> mete_consigliate;
	
	
	/**
	 * GETTERS AND SETTERS
	 */

	public String getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public int getAnni_esperienza() {
		return anni_esperienza;
	}

	public void setAnni_esperienza(int anni_esperienza) {
		this.anni_esperienza = anni_esperienza;
	}

	public List<MetaTuristica> getMete_consigliate() {
		return mete_consigliate;
	}

	public void setMete_consigliate(List<MetaTuristica> mete_consigliate) {
		this.mete_consigliate = mete_consigliate;
	}


}
