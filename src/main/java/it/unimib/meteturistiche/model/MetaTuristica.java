package it.unimib.meteturistiche.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class MetaTuristica {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long meta_id;

	@Column
	private String meta_nome;
	
	@Column
	private String meta_citta;
	
	@Column
	private String meta_paese;
	
	@Column
	private String meta_descrizione;
	
	@ManyToMany(mappedBy = "mete_future")
	private List<Utente> utenti_futuri;
	
	@ManyToMany(mappedBy = "mete_consigliate")
	private List<Guida> guide_affezionate;
	
	@ManyToMany(mappedBy = "mete_preferite")
	private List<Viaggiatore> viaggiatori_affezionati;
	
	@ManyToMany
	@JoinTable(
			  name = "meta_meta", 
			  joinColumns = @JoinColumn(name = "meta_id1"), 
			  inverseJoinColumns = @JoinColumn(name = "meta_id2"))
	private List<MetaTuristica> mete_simili;
		
    public MetaTuristica() {
    }
    
    /**
     * GETTERS AND SETTERS 
     */
    
	public Long getMeta_id() {
		return meta_id;
	}


	public void setMeta_id(Long meta_id) {
		this.meta_id = meta_id;
	}


	public String getMeta_nome() {
		return meta_nome;
	}


	public void setMeta_nome(String meta_nome) {
		this.meta_nome = meta_nome;
	}


	public String getMeta_citta() {
		return meta_citta;
	}


	public void setMeta_citta(String meta_citta) {
		this.meta_citta = meta_citta;
	}


	public String getMeta_paese() {
		return meta_paese;
	}


	public void setMeta_paese(String meta_paese) {
		this.meta_paese = meta_paese;
	}


	public String getMeta_descrizione() {
		return meta_descrizione;
	}


	public void setMeta_descrizione(String meta_descrizione) {
		this.meta_descrizione = meta_descrizione;
	}

	public List<Viaggiatore> getViaggiatori_affezionati() {
		return viaggiatori_affezionati;
	}

	public void setViaggiatori_affezionati(List<Viaggiatore> viaggiatori_affezionati) {
		this.viaggiatori_affezionati = viaggiatori_affezionati;
	}

	public List<Guida> getGuide_affezionate() {
		return guide_affezionate;
	}

	public void setGuide_affezionate(List<Guida> guide_affezionate) {
		this.guide_affezionate = guide_affezionate;
	}

	public List<MetaTuristica> getMete_simili() {
		return mete_simili;
	}

	public void setMete_simili(List<MetaTuristica> mete_simili) {
		this.mete_simili = mete_simili;
	}

	public List<Utente> getUtenti_futuri() {
		return utenti_futuri;
	}

	public void setUtenti_futuri(List<Utente> utenti_futuri) {
		this.utenti_futuri = utenti_futuri;
	}


}

