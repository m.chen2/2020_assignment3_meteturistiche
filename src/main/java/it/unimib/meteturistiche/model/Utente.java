package it.unimib.meteturistiche.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="categoria")
public class Utente {
	
	
	@Id
	@Column(name = "utente_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long utente_id;
	
	@Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;
    
    @Column(name = "e_mail")
    private String e_mail;

    @Column(name = "nazionalita")
    private String nazionalita;
    
    @Column(insertable = false, updatable = false)
    private String categoria;
    
	@ManyToMany
	@JoinTable(
			  name = "utente_hotel", 
			  joinColumns = @JoinColumn(name = "utente_id"), 
			  inverseJoinColumns = @JoinColumn(name = "hotel_id"))
	private List<Hotel> hotel_scelti;
	
	@ManyToMany
	@JoinTable(
			  name = "utente_meta", 
			  joinColumns = @JoinColumn(name = "utente_id"), 
			  inverseJoinColumns = @JoinColumn(name = "meta_id"))
	private List<MetaTuristica> mete_future;
    
    public Utente() {
    }
    
    /**
     * GETTERS AND SETTERS
     */
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getE_mail() {
		return e_mail;
	}

	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}

	public Long getUtente_id() {
		return utente_id;
	}


	public void setUtente_id(Long utente_id) {
		this.utente_id = utente_id;
	}


	public String getNazionalita() {
		return nazionalita;
	}


	public void setNazionalita(String nazionalita) {
		this.nazionalita = nazionalita;
	}


	public String getCategoria() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public List<Hotel> getHotel_scelti() {
		return hotel_scelti;
	}


	public void setHotel_scelti(List<Hotel> hotel_scelti) {
		this.hotel_scelti = hotel_scelti;
	}


	public List<MetaTuristica> getMete_future() {
		return mete_future;
	}


	public void setMete_future(List<MetaTuristica> mete_future) {
		this.mete_future = mete_future;
	}
	
	
}
