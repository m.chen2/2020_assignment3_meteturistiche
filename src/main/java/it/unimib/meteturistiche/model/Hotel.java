package it.unimib.meteturistiche.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Hotel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long hotel_id;
	
	@Column
	private String hotel_nome;
	
	@Column
	private String hotel_descrizione;
	
	@Column
	private String hotel_citta;
	
	@Column
	private int hotel_stelle;

	/**
	 * RELATIONSHIP
	 */

	@ManyToMany(mappedBy="hotel_scelti")
	private List<Utente> clienti_futuri;
	
	
	
	/**
	 * GETTERS AND SETTERS
	 */

	public Long getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(Long hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getHotel_nome() {
		return hotel_nome;
	}

	public void setHotel_nome(String hotel_nome) {
		this.hotel_nome = hotel_nome;
	}

	public String getHotel_descrizione() {
		return hotel_descrizione;
	}

	public void setHotel_descrizione(String hotel_descrizione) {
		this.hotel_descrizione = hotel_descrizione;
	}

	public String getHotel_citta() {
		return hotel_citta;
	}

	public void setHotel_citta(String hotel_citta) {
		this.hotel_citta = hotel_citta;
	}

	public int getHotel_stelle() {
		return hotel_stelle;
	}

	public void setHotel_stelle(int hotel_stelle) {
		this.hotel_stelle = hotel_stelle;
	}


	public List<Utente> getClienti_futuri() {
		return clienti_futuri;
	}

	public void setClienti_futuri(List<Utente> clienti_futuri) {
		this.clienti_futuri = clienti_futuri;
	}

	
	

}
