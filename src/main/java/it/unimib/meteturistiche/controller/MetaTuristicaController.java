package it.unimib.meteturistiche.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.unimib.meteturistiche.model.MetaTuristica;
import it.unimib.meteturistiche.repository.MetaTuristicaRepository;



@Controller
@RequestMapping("/mete")
public class MetaTuristicaController {
	
	@Autowired
	private MetaTuristicaRepository metaRepository;
	
	/*
	 READ
	*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView getMeteList() {
	    ModelAndView modelAndView = new ModelAndView("lista_mete");
	    modelAndView.addObject("listaMete", metaRepository.findAll() );
	    modelAndView.addObject("nuovaMeta",new MetaTuristica());
	    return modelAndView;
	}
	
	@RequestMapping(value="/{meta_id}", method=RequestMethod.GET)
	public ModelAndView getMeta(@PathVariable Long meta_id) {
		ModelAndView mav = new ModelAndView("dettagli_meta");
		MetaTuristica meta = metaRepository.getOne(meta_id);
		mav.addObject("meta",meta);
		mav.addObject("viaggiatoriAffezionati", meta.getViaggiatori_affezionati());
		mav.addObject("guideAffezionate", meta.getGuide_affezionate());
		mav.addObject("meteSimili", metaRepository.getMeteSimiliByMeta(meta_id));
		return mav;
	}
	
	/**
	 * Ricerca una meta tramite la città e/o la guida che la consiglia
	 * @param citta nome della città in cui deve risiedere la meta turistica
	 * @param guida nome della guida che consiglia la meta
	 * @return ModelAndView con la lista delle mete trovate
	 */
	@RequestMapping(value = "/ricerca_meta")
	public ModelAndView getMeteRicercate(@RequestParam ("citta") String citta, @RequestParam ("guida") String guida) {
		ModelAndView mav = new ModelAndView("lista_mete");
		
		List <MetaTuristica> meteRicercate = metaRepository.findMetaByCittaAndGuida(citta, guida);
		
		if(citta == "")
			meteRicercate = metaRepository.findMetaByGuida(guida);
		
		if(guida == "")
			meteRicercate = metaRepository.getByCitta(citta);
		
		mav.addObject("listaMete", meteRicercate);
		mav.addObject("nuovaMeta",new MetaTuristica());
		return mav; 
	}
	
	/*
	  CREATE
	*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public String createMeta(MetaTuristica meta, BindingResult binding) {
		metaRepository.save(meta);
		return "redirect:/mete";	
	}
	
	/*
	  ADD META SIMILE
	  Reindirizza alla pagina per aggiungere una meta simile
	*/	
	@RequestMapping(value="/aggiungi_meta_simile/{meta_id}")
	public ModelAndView addMetaSimile(@PathVariable Long meta_id) {
		ModelAndView mav = new ModelAndView("aggiungi_meta_simile");
		MetaTuristica metaPrincipale = metaRepository.getOne(meta_id);
		
		List <MetaTuristica> listaMete = metaRepository.findAll();
		listaMete.remove(metaPrincipale);
		listaMete.removeAll(metaRepository.getMeteSimiliByMeta(meta_id));
		
		mav.addObject("listaMete", listaMete);
		mav.addObject("metaPrincipale",metaPrincipale);
		return mav;
	}
	 /*
	  Effettua l'aggiunta della meta simile
	 */
	@RequestMapping(value="/aggiungi_meta_simile/{meta_id1}/{meta_id2}")
	public ModelAndView do_addMetaSimile(@PathVariable Long meta_id1, @PathVariable Long meta_id2) {
		metaRepository.aggiungiMetaSimile(meta_id1,meta_id2);		
		return new ModelAndView("redirect:/mete/" + meta_id1);
	}
	
	/*
	 UPDATE
	*/
	// Reindirizza alla pagina per la modifica
	@RequestMapping(value="/modifica_meta/{id}", method=RequestMethod.GET)
	public ModelAndView updateMeta(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView("modifica_meta");
		MetaTuristica meta = metaRepository.getOne(id);
		mav.addObject("meta", meta );
		mav.addObject("meteSimili", metaRepository.getMeteSimiliByMeta(id));
		return mav;
	}
	// Effettua la modifica	
	@RequestMapping(value="/modifica_meta", method=RequestMethod.POST)
	public ModelAndView do_updateMeta(MetaTuristica meta, BindingResult bindingResult) {
		metaRepository.update(meta.getMeta_id(),meta.getMeta_nome(),meta.getMeta_citta(),meta.getMeta_paese(),meta.getMeta_descrizione());
		//metaRepository.save(meta);
		return new ModelAndView("redirect:/mete");
	}
	
	
	/*
	  DELETE
	*/
	@RequestMapping(value="/elimina_meta/{id}")
	public ModelAndView deleteMeta(@PathVariable Long id) {
		
		metaRepository.deleteAsMetaSimile(id);
		metaRepository.deleteAsMetaPreferita(id);
		metaRepository.deleteAsMetaConsigliata(id);
		metaRepository.deleteAsMetaFutura(id);
		metaRepository.deleteById(id);
		
		return new ModelAndView("redirect:/mete");
	}
	
	
	
	
}
