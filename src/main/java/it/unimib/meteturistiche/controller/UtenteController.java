package it.unimib.meteturistiche.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import it.unimib.meteturistiche.model.MetaTuristica;
import it.unimib.meteturistiche.model.Utente;
import it.unimib.meteturistiche.model.Hotel;
import it.unimib.meteturistiche.repository.UtenteRepository;
import it.unimib.meteturistiche.repository.HotelRepository;
import it.unimib.meteturistiche.repository.MetaTuristicaRepository;

@Controller
@RequestMapping("/utenti")
public class UtenteController {
	
	@Autowired
	private UtenteRepository utenteRepository;
	
	@Autowired
	private HotelRepository hotelRepository;
	
	@Autowired
	private MetaTuristicaRepository metaRepository;
	
	/*
	  GET
	 */
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView getUtenteList() {
	    ModelAndView modelAndView = new ModelAndView("utenti");
	    modelAndView.addObject("utenti", utenteRepository.findAll() );
	    return modelAndView;
	}
	
	/**
	 * ADD HOTEL
	 * Reindirizza alla pagina per l'aggiunta dell'hotel alla propria lista
	 * @param utente_id ID dell'utente (guida o viaggiatore) che sta aggiungendo un hotel alla propria lista
	 * @return ModelAndView con la lista degli hotel che non ha ancora aggiunto
	 */
	@RequestMapping(value="/aggiungi_hotel/{utente_id}")
	public ModelAndView addHotel(@PathVariable Long utente_id) {
		ModelAndView mav = new ModelAndView("aggiungi_hotel_scelto");
		
		Utente utente = utenteRepository.getOne(utente_id);
		List <Hotel> listaHotelPossibili = hotelRepository.findAll();
		listaHotelPossibili.removeAll(utente.getHotel_scelti());
		
		mav.addObject("utente", utente );
		mav.addObject("listaHotel",  listaHotelPossibili );
		return mav;
		
	}
	// Effettua l'aggiunta dell'hotel
	@RequestMapping(value="/aggiungi_hotel/{utente_id}/{hotel_id}")
	public ModelAndView do_addHotel(@PathVariable Long utente_id, @PathVariable Long hotel_id) {
		utenteRepository.aggiungiHotel(utente_id,hotel_id);		
		return new ModelAndView("redirect:/utenti/{utente_id}");
	}
	
	
	/**
	 * ADD META FUTURA
	 * Reindirizza alla pagina per l'aggiunta della meta futura
	 * @param utente_id ID dell'utente (guida o viaggiatore) che sta aggiungendo una meta che visiterà
	 * @return ModelAndView con la lista delle mete che visiterà
	*/
	@RequestMapping(value="/aggiungi_meta_futura/{utente_id}")
	public ModelAndView addMetaFutura(@PathVariable Long utente_id) {
		ModelAndView mav = new ModelAndView("aggiungi_meta_futura");
		
		Utente utente = utenteRepository.getOne(utente_id);
		List <MetaTuristica> listaMeteFuturePossibili = metaRepository.findAll();
		listaMeteFuturePossibili.removeAll(utente.getMete_future());
		
		mav.addObject("utente", utente );
		mav.addObject("listaMete", listaMeteFuturePossibili );
		return mav;
		
	}
	
	// Effettua l'aggiunta della meta futura
	@RequestMapping(value="/aggiungi_meta_futura/{utente_id}/{meta_id}")
	public ModelAndView do_addMetaFutura(@PathVariable Long utente_id, @PathVariable Long meta_id) {
		utenteRepository.addMetaFutura(utente_id,meta_id);		
		return new ModelAndView("redirect:/utenti/{utente_id}");
	}
	
	/*
	 Reindirizza alla pagina in base se l'utente è una guida o un viaggiatore
	*/
	@RequestMapping(value="/{utente_id}", method=RequestMethod.GET)
	public ModelAndView getUtente(@PathVariable Long utente_id) {
		Utente utente = utenteRepository.getOne(utente_id);
		ModelAndView mav = new ModelAndView();
		
		if(utente.getCategoria().equalsIgnoreCase("viaggiatore")) {
			mav.setViewName("redirect:/utenti/viaggiatori/" + utente_id);
			return mav;
		}else if(utente.getCategoria().equalsIgnoreCase("guida")) {
			mav.setViewName("redirect:/utenti/guide/" + utente_id);
			return mav;
		}
		
		mav.setViewName("/utenti");
		return mav;
	}
	
	
	/*
	 DELETE
	*/
	@RequestMapping(value="/elimina_utente/{id}")
	public ModelAndView deleteUtente(@PathVariable Long id) {
		utenteRepository.deleteById(id);
		return new ModelAndView("redirect:/utenti");
	}
	

}
