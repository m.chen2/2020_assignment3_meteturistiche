package it.unimib.meteturistiche.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import it.unimib.meteturistiche.model.MetaTuristica;
import it.unimib.meteturistiche.model.Viaggiatore;
import it.unimib.meteturistiche.model.Hotel;
import it.unimib.meteturistiche.repository.MetaTuristicaRepository;
import it.unimib.meteturistiche.repository.ViaggiatoreRepository;

@Controller
@RequestMapping("/utenti/viaggiatori")
public class ViaggiatoreController {
	
	@Autowired
	private ViaggiatoreRepository viaggiatoreRepository;
	
	@Autowired
	private MetaTuristicaRepository metaTuristicaRepository;
	
	/*
	 READ
	*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView getViaggiatoriList() {
	    ModelAndView modelAndView = new ModelAndView("lista_viaggiatori");
	    modelAndView.addObject("listaViaggiatori", viaggiatoreRepository.findAll() );
	    modelAndView.addObject("nuovoViaggiatore", new Viaggiatore());
	    return modelAndView;
	}
	
	@RequestMapping(value="/{viaggiatore_id}", method=RequestMethod.GET)
	public ModelAndView getViaggiatore(@PathVariable Long viaggiatore_id) {
		ModelAndView mav = new ModelAndView("dettagli_viaggiatore");
		Viaggiatore viaggiatore = viaggiatoreRepository.getOne(viaggiatore_id);
		
		mav.addObject("hotelsScelti", viaggiatore.getHotel_scelti());
		mav.addObject("metePreferite", viaggiatore.getMete_preferite());
		mav.addObject("meteFuture", viaggiatore.getMete_future());
		mav.addObject("viaggiatore", viaggiatore );
		return mav;
	}

	/**
	 * ADD META PREFERITA
	 * Reindirizza alla pagina per aggiungere una meta preferita 
	 * mostrando le mete che non ha ancora consigliato
	 * @param viaggiatore_id ID del viaggiatore che sta aggiungendo un meta ai propri preferiti
	 * @return ModelAndView con la lista delle mete non ancora presente nella propria lista preferiti
	*/
	@RequestMapping(value="/aggiungi_meta_preferita/{viaggiatore_id}")
	public ModelAndView addMetaPreferita(@PathVariable Long viaggiatore_id) {
		ModelAndView mav = new ModelAndView("aggiungi_meta_preferita");
		
		Viaggiatore viaggiatore = viaggiatoreRepository.getOne(viaggiatore_id);
		List <MetaTuristica> listaMetePreferitePossibili = metaTuristicaRepository.findAll();
		listaMetePreferitePossibili.removeAll(viaggiatore.getMete_preferite());
		
		mav.addObject("viaggiatore", viaggiatore );
		mav.addObject("listaMete", listaMetePreferitePossibili );
		return mav;
		
	}
	//Effettua l'aggiunta della meta preferita
	@RequestMapping(value="/aggiungi_meta_preferita/{viaggiatore_id}/{meta_id}")
	public ModelAndView do_addMetaPreferita(@PathVariable Long viaggiatore_id, @PathVariable Long meta_id) {
		viaggiatoreRepository.aggiungiMetaPreferita(viaggiatore_id,meta_id);		
		return new ModelAndView("redirect:/utenti/viaggiatori/" + viaggiatore_id);
	}
	
	/*
	  CREATE
	*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public String createViaggiatore(Viaggiatore viaggiatore, BindingResult binding) {
		viaggiatoreRepository.save(viaggiatore);
		return "redirect:/utenti/viaggiatori";
	}
	
	/*
	 UPDATE
	*/
	@RequestMapping(value="/modifica_viaggiatore/{viaggiatore_id}", method=RequestMethod.GET)
	public ModelAndView updateViaggiatore(@PathVariable Long viaggiatore_id) {
		ModelAndView mav = new ModelAndView("modifica_viaggiatore");
		Viaggiatore viaggiatore = viaggiatoreRepository.getOne(viaggiatore_id);
		
		List <MetaTuristica> metePreferite = viaggiatore.getMete_preferite();
		List <Hotel> hotelsScelti = viaggiatore.getHotel_scelti();
		List <MetaTuristica> meteFuture = viaggiatore.getMete_future();

		mav.addObject("hotelsScelti", hotelsScelti);
		mav.addObject("metePreferite", metePreferite);
		mav.addObject("meteFuture", meteFuture);
		mav.addObject("viaggiatore", viaggiatore );
		return mav;
	}
	
	//Effettua la modifica
	@RequestMapping(value="/modifica_viaggiatore", method=RequestMethod.POST)
	public ModelAndView do_updateViaggiatore(Viaggiatore viaggiatore, BindingResult bindingResult) {
		
		viaggiatoreRepository.update(viaggiatore.getUtente_id(),viaggiatore.getNome(),viaggiatore.getCognome(),
								viaggiatore.getNazionalita(),viaggiatore.getE_mail(),viaggiatore.getN_viaggi());
		//viaggiatoreRepository.save(viaggiatore);
		return new ModelAndView("redirect:/utenti/viaggiatori");
	}
	
	
	/*
	  DELETE
	*/
	@RequestMapping(value="/elimina_viaggiatore/{id}")
	public ModelAndView deleteViaggiatore(@PathVariable Long id) {
		viaggiatoreRepository.deleteById(id);
		return new ModelAndView("redirect:/utenti/viaggiatori");
	}
	
	

}
