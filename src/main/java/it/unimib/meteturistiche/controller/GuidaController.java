package it.unimib.meteturistiche.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import it.unimib.meteturistiche.model.Hotel;
import it.unimib.meteturistiche.model.MetaTuristica;
import it.unimib.meteturistiche.model.Guida;
import it.unimib.meteturistiche.repository.MetaTuristicaRepository;
import it.unimib.meteturistiche.repository.GuidaRepository;

@Controller
@RequestMapping("/utenti/guide")
public class GuidaController {

	
	@Autowired
	private GuidaRepository guidaRepository;
	
	@Autowired
	private MetaTuristicaRepository metaTuristicaRepository;
	
	
	/*
	 * READ
	 */
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView getGuideList() {
	    ModelAndView modelAndView = new ModelAndView("lista_guide");
	    modelAndView.addObject("listaGuide", guidaRepository.findAll() );
	    modelAndView.addObject("nuovaGuida", new Guida());
	    return modelAndView;
	}
	
	@RequestMapping(value="/{guida_id}", method=RequestMethod.GET)
	public ModelAndView getGuida(@PathVariable Long guida_id) {
		ModelAndView mav = new ModelAndView("dettagli_guida");
		Guida guida = guidaRepository.getOne(guida_id);
		
		mav.addObject("hotelsScelti", guida.getHotel_scelti());
		mav.addObject("meteConsigliate", guida.getMete_consigliate());
		mav.addObject("meteFuture", guida.getMete_future());
		mav.addObject("guida", guida );
		return mav;
	}

	/**
	 * ADD META CONSIGLIATA
	 * Reindirizza alla pagina per all'aggiunta della meta consigliata 
	 * mostrando tutte le mete tranne quelle che ha già consigliato
	 * @param guida_id ID della guida che sta consigliando la meta
	 * @return ModelAndView con la lista delle mete che non ha ancora consigliato
	 */
	@RequestMapping(value="/aggiungi_meta_consigliata/{guida_id}")
	public ModelAndView addMetaConsigliata(@PathVariable Long guida_id) {
		ModelAndView mav = new ModelAndView("aggiungi_meta_consigliata");
		
		Guida guida = guidaRepository.getOne(guida_id);
		List <MetaTuristica> listaMeteConsigliatePossibili = metaTuristicaRepository.findAll();
		listaMeteConsigliatePossibili.removeAll(guida.getMete_consigliate());
		
		mav.addObject("guida", guidaRepository.getOne(guida_id) );
		mav.addObject("listaMete", listaMeteConsigliatePossibili);
		
		return mav;
	}
	// Effettua l'aggiunta della meta consigliata
	@RequestMapping(value="/aggiungi_meta_consigliata/{guida_id}/{meta_id}")
	public ModelAndView do_addMetaConsigliata(@PathVariable Long guida_id, @PathVariable Long meta_id) {
		guidaRepository.aggiungiMetaConsigliata(guida_id,meta_id);		
		return new ModelAndView("redirect:/utenti/guide/" + guida_id);
	}
	
	
	/*
	 * CREATE
	 */
	@RequestMapping(value="", method=RequestMethod.POST)
	public String createGuida(Guida guida, BindingResult binding) {
		guidaRepository.save(guida);
		return "redirect:/utenti/guide";
	}
	
	/*
	 * UPDATE
	 */
	//Reindirizza alla pagina per la modifica
	@RequestMapping(value="/modifica_guida/{guida_id}", method=RequestMethod.GET)
	public ModelAndView updateGuida(@PathVariable Long guida_id) {
		ModelAndView mav = new ModelAndView("modifica_guida");
		Guida guida = guidaRepository.getOne(guida_id);
		
		List <MetaTuristica> meteConsigliate = guida.getMete_consigliate();
		List <Hotel> hotelsScelti = guida.getHotel_scelti();
		List <MetaTuristica> meteFuture = guida.getMete_future();
		
		mav.addObject("hotelsScelti", hotelsScelti);
		mav.addObject("meteConsigliate", meteConsigliate);
		mav.addObject("meteFuture", meteFuture);
		mav.addObject("guida", guida );
		return mav;
	}
	//Effettua la modifica
	@RequestMapping(value="/modifica_guida", method=RequestMethod.POST)
	public ModelAndView do_updateGuida(Guida guida, BindingResult bindingResult) {
		guidaRepository.update(guida.getUtente_id(),guida.getNome(),guida.getCognome(),
				guida.getNazionalita(),guida.getE_mail(),guida.getAgenzia(),guida.getAnni_esperienza());
		return new ModelAndView("redirect:/utenti/guide");
	}
	
	/*
	 * DELETE
	 */
	@RequestMapping(value="/elimina_guida/{id}")
	public ModelAndView deleteGuida(@PathVariable Long id) {
		guidaRepository.deleteById(id);
		return new ModelAndView("redirect:/utenti/guide");
	}
	

}
