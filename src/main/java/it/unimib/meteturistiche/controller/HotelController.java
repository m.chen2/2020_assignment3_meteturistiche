package it.unimib.meteturistiche.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import it.unimib.meteturistiche.model.Hotel;
import it.unimib.meteturistiche.repository.HotelRepository;

@Controller
@RequestMapping("/hotel")
public class HotelController {
	
	
	
	@Autowired
	private HotelRepository hotelRepository;
	
	/*
	 READ
	*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView getHotelList() {
	    ModelAndView modelAndView = new ModelAndView("lista_hotel");
	    modelAndView.addObject("listaHotel", hotelRepository.findAll() );
	    modelAndView.addObject("nuovoHotel",new Hotel());
	    return modelAndView;
	}
	
	@RequestMapping(value="/{hotel_id}", method=RequestMethod.GET)
	public ModelAndView getHotel(@PathVariable Long hotel_id) {
		ModelAndView mav = new ModelAndView("dettagli_hotel");
		Hotel hotel = hotelRepository.getOne(hotel_id);
		mav.addObject("clienti_futuri",hotel.getClienti_futuri());
		mav.addObject("hotel", hotel);
		return mav;
	}
	
	/*
	 CREATE
	*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public String createHotel(Hotel hotel, BindingResult binding) {
		hotelRepository.save(hotel);
		return "redirect:/hotel";
	}
	
	/*
	  UPDATE
	  Reindirizza alla pagina della modifica
	*/
	@RequestMapping(value="/modifica_hotel/{hotel_id}", method=RequestMethod.GET)
	public ModelAndView updateHotel(@PathVariable Long hotel_id) {
		ModelAndView mav = new ModelAndView("modifica_hotel");
		Hotel hotel = hotelRepository.getOne(hotel_id);
		mav.addObject("hotel", hotel );
		return mav;
	}
		
	//Effettua la modifica
	@RequestMapping(value="/modifica_hotel", method=RequestMethod.POST)
	public ModelAndView do_updateHotel(Hotel hotel, BindingResult bindingResult) {
		hotelRepository.save(hotel);
		return new ModelAndView("redirect:/hotel");
	}
	
	
	/*
	  DELETE
	*/
	@RequestMapping(value="/elimina_hotel/{hotel_id}")
	public ModelAndView deleteHotel(@PathVariable Long hotel_id) {
		hotelRepository.deleteAsHotelScelto(hotel_id);
		hotelRepository.deleteById(hotel_id);
		
		return new ModelAndView("redirect:/hotel");
	}

}
