package it.unimib.meteturistiche.repository;

import it.unimib.meteturistiche.model.MetaTuristica;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MetaTuristicaRepository extends JpaRepository <MetaTuristica, Long> {
	
	@Query(value="SELECT meta.* \r\n"
			+ "			FROM META_META ms \r\n"
			+ "			JOIN META_TURISTICA meta on ms.META_ID2 = meta.META_ID \r\n"
			+ "			WHERE ms.META_ID1 = ?1 \r\n"
			+ "UNION\r\n"
			+ "SELECT meta.* \r\n"
			+ "			FROM META_META ms \r\n"
			+ "			JOIN META_TURISTICA meta on ms.META_ID1 = meta.META_ID \r\n"
			+ "			WHERE ms.META_ID2 =  ?1 " , nativeQuery=true)
	List<MetaTuristica> getMeteSimiliByMeta(Long id);
	
	
	@Modifying
	@Query(value = "INSERT INTO META_META(META_ID1,META_ID2) \r\n"
			+ "VALUES (?1,?2)", nativeQuery=true)
	@Transactional
	void aggiungiMetaSimile(Long meta_id1, Long meta_id2);

	
	@Modifying
	@Query(value = "DELETE FROM VIAGGIATORE_META WHERE META_ID = ?1" , nativeQuery=true)
	@Transactional
	void deleteAsMetaPreferita(Long id);
	
	@Modifying
	@Query(value = "DELETE FROM META_META WHERE META_ID1 = ?1 OR META_ID2 = ?1" , nativeQuery=true)
	@Transactional
	void deleteAsMetaSimile(Long id);
	
	@Modifying
	@Query(value = "DELETE FROM GUIDA_META WHERE META_ID = ?1" , nativeQuery=true)
	@Transactional
	void deleteAsMetaConsigliata(Long id);
	
	@Modifying
	@Query(value = "DELETE FROM UTENTE_META WHERE META_ID = ?1" , nativeQuery=true)
	@Transactional
	void deleteAsMetaFutura(Long id);
	

	@Query(value = "SELECT meta.* \r\n"
			+ "FROM GUIDA_META mc \r\n"
			+ "JOIN META_TURISTICA meta on mc.META_ID = meta.META_ID\r\n"
			+ "JOIN UTENTE guida on guida.UTENTE_ID = mc.GUIDA_ID \r\n"
			+ "WHERE UPPER(meta.meta_citta) = UPPER(?1) AND UPPER(guida.NOME) = UPPER(?2)" , nativeQuery=true)
	List<MetaTuristica> findMetaByCittaAndGuida(String citta, String guida);
	
	
	@Query(value = "SELECT meta.* \r\n"
			+ "FROM GUIDA_META mc \r\n"
			+ "JOIN META_TURISTICA meta on mc.META_ID = meta.META_ID\r\n"
			+ "JOIN UTENTE guida on guida.UTENTE_ID = mc.GUIDA_ID \r\n"
			+ "WHERE UPPER(guida.NOME) = UPPER(?1)" , nativeQuery=true)
	List<MetaTuristica> findMetaByGuida(String guida);

	@Query(value = "SELECT meta.* FROM META_TURISTICA  meta WHERE UPPER(meta.meta_citta) = UPPER(?1)", nativeQuery=true)
	List<MetaTuristica> getByCitta(String citta);
	
	@Modifying
	@Query(value = "UPDATE META_TURISTICA SET meta_nome=?2, meta_citta=?3,meta_paese=?4, meta_descrizione=?5 WHERE META_ID = ?1" , nativeQuery=true)
	@Transactional
	void update(Long meta_id, String meta_nome, String meta_citta, String meta_paese, String meta_descrizione);
	
	

}



