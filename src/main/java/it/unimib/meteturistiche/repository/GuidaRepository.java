package it.unimib.meteturistiche.repository;

import it.unimib.meteturistiche.model.Guida;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GuidaRepository extends JpaRepository <Guida, Long> {
	
	@Modifying
	@Query(value = "INSERT INTO GUIDA_META(GUIDA_ID,META_ID) \r\n"
			+ "VALUES (?1,?2)", nativeQuery=true)
	@Transactional
	void aggiungiMetaConsigliata(Long guida_id, Long meta_id);

	@Modifying
	@Query(value = "UPDATE UTENTE u SET \r\n"
			+ "u.nome = ?2, u.cognome = ?3, u.nazionalita = ?4, u.e_mail=?5, u.agenzia=?6, u.anni_esperienza=?7 WHERE u.utente_id = ?1", nativeQuery = true)
	@Transactional
	void update(Long utente_id, String nome, String cognome, String nazionalita, String e_mail, String agenzia, int anni_esperienza);

}