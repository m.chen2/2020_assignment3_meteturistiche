package it.unimib.meteturistiche.repository;

import it.unimib.meteturistiche.model.Utente;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UtenteRepository extends JpaRepository <Utente, Long> {
	
	
	@Modifying
	@Query(value = "INSERT INTO UTENTE_HOTEL(UTENTE_ID,HOTEL_ID) \r\n"
			+ "VALUES (?1,?2)", nativeQuery=true)
	@Transactional
	void aggiungiHotel(Long utente_id, Long hotel_id);

	
	@Query(value = "SELECT u.* \r\n"
			+ "	FROM UTENTE_HOTEL hs \r\n"
			+ "	JOIN utente u on u.utente_id = hs.utente_id \r\n"
			+ "	WHERE hs.hotel_id = ?1", nativeQuery=true)
	List<Utente> getClientiFuturiByHotel(Long hotel_id);

	@Modifying
	@Query(value = "INSERT INTO UTENTE_META(UTENTE_ID,META_ID) \r\n"
			+ "VALUES (?1,?2)", nativeQuery=true)
	@Transactional
	void addMetaFutura(Long utente_id, Long meta_id); 


}