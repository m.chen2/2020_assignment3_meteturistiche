package it.unimib.meteturistiche.repository;

import it.unimib.meteturistiche.model.Viaggiatore;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

@Repository
public interface ViaggiatoreRepository extends JpaRepository <Viaggiatore, Long> {
	
	@Modifying
	@Query(value = "INSERT INTO VIAGGIATORE_META(VIAGGIATORE_ID,META_ID) \r\n"
			+ "VALUES (?1,?2)", nativeQuery=true)
	@Transactional
	void aggiungiMetaPreferita(Long viaggiatore_id, Long meta_id);

	@Modifying
	@Query(value = "UPDATE UTENTE u SET u.nome = ?2, u.cognome = ?3, u.nazionalita = ?4, u.e_mail=?5, u.n_viaggi=?6 WHERE u.utente_id = ?1", nativeQuery = true)
	@Transactional
	void update(Long utente_id, String nome, String cognome, String nazionalita, String e_mail, int n_viaggi);


}