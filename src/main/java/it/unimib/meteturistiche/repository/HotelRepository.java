package it.unimib.meteturistiche.repository;

import java.util.List;

import javax.transaction.Transactional;

import it.unimib.meteturistiche.model.Hotel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepository extends JpaRepository <Hotel , Long> {
	
	@Query(value = "SELECT h.* FROM HOTEL h WHERE h.hotel_citta = ?1", nativeQuery=true)
	List<Hotel> getHotelByCitta(String string);

	@Modifying
	@Query(value = "DELETE FROM UTENTE_HOTEL WHERE HOTEL_ID = ?1" , nativeQuery=true)
	@Transactional
	void deleteAsHotelScelto(Long hotel_id);

}
